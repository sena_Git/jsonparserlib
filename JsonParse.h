#ifndef __JSON_LIB_CLASS_H_
#define __JSON_LIB_CLASS_H_

#include "jsmn.h"
#include <stdlib.h>
#include <string.h>

#define JP_MAX_TOKENCOUNT (256)

namespace SAREA
{

    struct JP_TokCurRange
    {
        size_t tokenIndexStart;
        size_t tokenCount;
    };

    class JsonParser
    {
    private:
        jsmn_parser             m_parser;

        static const size_t     m_maxTokenCount = JP_MAX_TOKENCOUNT;
        jsmntok_t               m_tokens[m_maxTokenCount];

        char                   *m_jsonSource;

        struct JP_TokCurRange searchTokCurRange( size_t tokenIndex );

    public:

        JsonParser( void );

        void JsonParserDtor( void );


        struct JP_TokCurRange parse( const char *jsonString );

        struct JP_TokCurRange getToken( struct JP_TokCurRange obj, const char *key );
        struct JP_TokCurRange getArrayToken( struct JP_TokCurRange obj, size_t index );


        int getInt( struct JP_TokCurRange obj, int defalt );
        int getInt( struct JP_TokCurRange obj, const char *key, int defalt );

        bool getFlag( struct JP_TokCurRange obj, bool defalt );
        bool getFlag( struct JP_TokCurRange obj, const char *key, bool defalt );

        float getFloat( struct JP_TokCurRange obj, float defalt );
        float getFloat( struct JP_TokCurRange obj, const char *key, float defalt );

        size_t getStringLen( struct JP_TokCurRange obj );
        void getString( struct JP_TokCurRange obj, char* buffer, size_t copyLen );
    };
}

typedef struct SAREA::JP_TokCurRange   JP_Object;


#endif
