#include "JsonParse.h"

namespace SAREA
{


JsonParser::JsonParser( void )
{
}


void JsonParser::JsonParserDtor( void )
{
    if( m_jsonSource != NULL )
    {
        delete m_jsonSource;
        m_jsonSource = NULL;
    }
}



struct JP_TokCurRange JsonParser::parse( const char *jsonString )
{
    struct JP_TokCurRange range;
    int parseResault;

    size_t jsonStrLen = strlen( jsonString );
    m_jsonSource = new char[jsonStrLen+1];
    memcpy( m_jsonSource, jsonString, jsonStrLen );
    m_jsonSource[jsonStrLen] = '\0';

    memset( m_tokens, 0x00, sizeof(m_tokens) );
    jsmn_init ( &m_parser );

    parseResault = jsmn_parse ( &m_parser, m_jsonSource, jsonStrLen, m_tokens, m_maxTokenCount );
    if( parseResault == JSMN_ERROR_INVAL || parseResault == JSMN_ERROR_PART )
    {
        //RT_dege( "Inval json format", 0 );
        range.tokenCount = 0;
        return range;
    }else if( parseResault  == JSMN_ERROR_NOMEM )
    {
        //RT_dege( "Insufficient m_maxTokenCount", 0 );
        range.tokenCount = 0;
        return range;
    }

    range.tokenIndexStart = 1;
    range.tokenCount = m_parser.toknext-2;
    return range;
}
    

struct JP_TokCurRange JsonParser::getToken( struct JP_TokCurRange obj, const char *key )
{
    size_t tokenLen = strlen( key );
    size_t tokenIndexStart = 0;

    for ( size_t i=obj.tokenIndexStart; i < obj.tokenIndexStart+obj.tokenCount; i++ )
    {
        if ( m_tokens[i].type == JSMN_KEY )
        {
            size_t keyLength = (size_t)(m_tokens[i].end-m_tokens[i].start);
            if ( (tokenLen == keyLength) && (strncmp( &m_jsonSource[m_tokens[i].start], key, keyLength )== 0) )
            {
                tokenIndexStart = i+1;
                break;
            }
        }
    }
    if (tokenIndexStart == 0)
    {
    	obj.tokenCount = 0;
        return obj;
    }

    return searchTokCurRange( tokenIndexStart );
}


struct JP_TokCurRange JsonParser::getArrayToken( struct JP_TokCurRange obj, size_t index )
{
    struct JP_TokCurRange targetToken = {obj.tokenIndexStart, 1};

    for( size_t i=0; i<=index; i++ )
    {
        targetToken = searchTokCurRange( targetToken.tokenIndexStart+targetToken.tokenCount );
    }

    return targetToken;
}



int JsonParser::getInt( struct JP_TokCurRange obj, int defalt )
{
    int resault = defalt;

    if ( (obj.tokenCount == 1) && (m_tokens[obj.tokenIndexStart].type == JSMN_PRIMITIVE) )
    {
        char buffer[64];
        memcpy( buffer, &m_jsonSource[m_tokens[obj.tokenIndexStart].start], sizeof(char)*(m_tokens[obj.tokenIndexStart].end - m_tokens[obj.tokenIndexStart].start) );
        buffer[m_tokens[obj.tokenIndexStart].end - m_tokens[obj.tokenIndexStart].start] = 0x00;

        resault = atoi(buffer);
    }

    return resault;
}

int JsonParser::getInt( struct JP_TokCurRange obj, const char *key, int defalt )
{
    return getInt( getToken( obj, key ), defalt );
}


bool JsonParser::getFlag( struct JP_TokCurRange obj, bool defalt )
{
    bool resault = defalt;

    if ( (obj.tokenCount == 1) && (m_tokens[obj.tokenIndexStart].type == JSMN_PRIMITIVE) )
    {
        if( m_jsonSource[m_tokens[ obj.tokenIndexStart].start] == 't' )
        {
            resault = true;
        }
        else if( m_jsonSource[m_tokens[ obj.tokenIndexStart].start] == 'f' )
        {
            resault = false;
        }

    }

    return resault;
}

bool JsonParser::getFlag( struct JP_TokCurRange obj, const char *key, bool defalt )
{
    return getFlag( getToken( obj, key ), defalt );
}



float JsonParser::getFloat( struct JP_TokCurRange obj, float defalt )
{
    float resault = defalt;

    if ( (obj.tokenCount == 1) && (m_tokens[obj.tokenIndexStart].type == JSMN_PRIMITIVE) )
    {
        char buffer[64];
        memcpy(buffer, &m_jsonSource[m_tokens[obj.tokenIndexStart].start], sizeof(char)*(m_tokens[obj.tokenIndexStart].end - m_tokens[obj.tokenIndexStart].start));
        buffer[m_tokens[obj.tokenIndexStart].end - m_tokens[obj.tokenIndexStart].start] = 0x00;

        resault = atoff( buffer );
    }

    return resault;
}

float JsonParser::getFloat( struct JP_TokCurRange obj, const char *key, float defalt )
{
    return getFloat( getToken( obj, key ), defalt );
}


size_t JsonParser::getStringLen( struct JP_TokCurRange obj )
{
    size_t resault = 0;

    if ( (obj.tokenCount == 1) && (m_tokens[obj.tokenIndexStart].type == JSMN_STRING) )
    {
        resault = m_tokens[obj.tokenIndexStart].end - m_tokens[obj.tokenIndexStart].start;
    }

    return resault;
}


void JsonParser::getString( struct JP_TokCurRange obj, char* buffer, size_t copyLen )
{
    if ( (obj.tokenCount == 1) && (m_tokens[obj.tokenIndexStart].type == JSMN_STRING) )
    {
        if( (m_tokens[obj.tokenIndexStart].end - m_tokens[obj.tokenIndexStart].start) < ((int)copyLen-1) )
        {
            copyLen = m_tokens[obj.tokenIndexStart].end - m_tokens[obj.tokenIndexStart].start;
        }
        copyLen -= 1;

        memcpy(buffer, &m_jsonSource[m_tokens[obj.tokenIndexStart].start], sizeof(char)*copyLen);
        buffer[copyLen] = 0x00;
    }
}



struct JP_TokCurRange JsonParser::searchTokCurRange( size_t tokenIndex )
{
    struct JP_TokCurRange range = {tokenIndex, m_parser.toknext-tokenIndex};
    int parent = m_tokens[tokenIndex].parent;

    for( size_t i=1; i<m_parser.toknext-tokenIndex; i++ )
    {
        if( m_tokens[tokenIndex+i].parent <= parent )
        {
            range.tokenCount = i;//-1;
            break;
        }
    }

    return range;
}

}
